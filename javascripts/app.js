;(function ($, window, undefined) {
  'use strict';

  var $doc = $(document),
      Modernizr = window.Modernizr;

  $(document).ready(function() {
    $.fn.foundationAlerts           ? $doc.foundationAlerts() : null;
    $.fn.foundationButtons          ? $doc.foundationButtons() : null;
    $.fn.foundationAccordion        ? $doc.foundationAccordion() : null;
    $.fn.foundationNavigation       ? $doc.foundationNavigation() : null;
    $.fn.foundationTopBar           ? $doc.foundationTopBar() : null;
    $.fn.foundationCustomForms      ? $doc.foundationCustomForms() : null;
    $.fn.foundationMediaQueryViewer ? $doc.foundationMediaQueryViewer() : null;
    $.fn.foundationTabs             ? $doc.foundationTabs({callback : $.foundation.customForms.appendCustomMarkup}) : null;
    $.fn.foundationTooltips         ? $doc.foundationTooltips() : null;
    $.fn.foundationMagellan         ? $doc.foundationMagellan() : null;
    $.fn.foundationClearing         ? $doc.foundationClearing() : null;

    $.fn.placeholder                ? $('input, textarea').placeholder() : null;
  });

  // UNCOMMENT THE LINE YOU WANT BELOW IF YOU WANT IE8 SUPPORT AND ARE USING .block-grids
  // $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
  // $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
  // $('.block-grid.four-up>li:nth-child(4n+1)').css({clear: 'both'});
  // $('.block-grid.five-up>li:nth-child(5n+1)').css({clear: 'both'});

  // Hide address bar on mobile devices (except if #hash present, so we don't mess up deep linking).
  if (Modernizr.touch && !window.location.hash) {
    $(window).load(function () {
      setTimeout(function () {
        window.scrollTo(0, 1);
      }, 0);
    });
  }

  function home() {
    $('body.index').css('min-height', ($(window).height()) );
    $('section.index').css('min-height', ($(window).height() - 150) );
    $('.wrapper').css('min-height', ($(window).height() - 60) );
  }

  $(document).ready(function() {
    home();
    $('section.index').css({backgroundSize: "cover"});
    $('.date-pick').fdatepicker({format: 'dd/mm/yyyy'});

    var menu = $('.mobile .five-up').height();
    var status = 'closed';

    $('.butterfly').click(function() {
      if (status === 'closed') {
        $('.mobile').css('height', menu );
        status = 'open';
      } else {
        $('.mobile').css('height', '0' );
        status = 'closed';
      }
    });

    $('tr.accordion .button.dropdown').click(function() {
      var type = $(this).parent().parent().attr('id');

      if ( $('tr.' + type + '-archive').css('display') === 'none' ) {
        $('tr.' + type + '-archive').css('display', 'table-row');
      } else {
        $('tr.' + type + '-archive').css('display', 'none');
      }
    });

  });

  $(window).resize(function() {
    home();
    var menu = $('.mobile .five-up').height();
    var status = 'closed';

    $('.butterfly').click(function() {
      if (status === 'closed') {
        $('.mobile').css('height', menu );
        status = 'open';
      } else {
        $('.mobile').css('height', '0' );
        status = 'closed';
      }
    });
  });


})(jQuery, this);
