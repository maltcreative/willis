###Second set of updates Jan 29

* Header reduced height
* Form Headings color changed
* Dropdowns made shorter 
* Font sizes changed globally, most fonts 13px, headings 20px
* Equalized margins on tables
* Cleaned formatting on tables

###Updates Jan 29

* IE bug fixed- Tested in IE8+
* Menu reduced further, icons and padding
* Headings reduced 75%, small icons
* Padding on tops of forms reduced 75%
* Input heights reduced by 33%
* Table font sizes reduced to 14px
* Table padding reduced by over 75%
* Table buttons reduced in size to fit inline
* Appointment bottom icons smaller, padding reduced
* Administrators Email removed from Profile signup
* Lender Trustee removed from index page and added to Profile signup
* Added Adminstrators section to Manage Company Details page
* Current Appointments, Ammend Appointment- Type of cover is now non-editable
* Copyright changed to 2015

###Updates Jan 28

* Times New Roman font REMOVED
* Reduced height of Header
* Reduced size of Main Navigation (icons and overall height)
* Pointer Cursor removed from input label
* Dropdowns added to New Appointment (General Information & Property Details)
* Added accordion style dropdowns to Reports page

##Template Listing

* Index `index.html`
* New User `profile.html` `profile-error.html`
* Dashboard `home.html`
* New Appointment Form `new-appointment.html (pt 1-5)`
* Appointments `appointment.html` `appointment-amend.html` `appointment-cancel.html` `appointment-claim.html` `appointment-renew.html`
* Reports `reports.html`
* Settings `settings-profile.html` `settings-manage.html` `settings-company.html`